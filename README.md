# Using-String-Resources-in-Java-Code-in-Android
/Read a simple string and set it in a text view
String simpleString = activity.getString(R.string.simple_string);
textView.setText(simpleString);

// Read a quoted string and set it in a text view
String quotedString = activity.getString(R.string.quoted_string);
textView.setText(quotedString);

//Read a double quoted string and set it in a text view
String doubleQuotedString = activity.getString(R.string.double_qouted_string);
textView.setText(doubleQoutedString);

//Read a Java format string
//Convert the formatted string by passing in arguments
String substitutedString = String.format(javaFormatString, "Hello", "Android");
//set the output in a text view
textView.setText(substitutedString);

//Read an html string from the resource and set it in a text view
String htmlTaggedString = activity.getString(R.string.tagged_string);
// Convert it to a text span so that it can be set in a text view
//android.text.Html class allows painting of "html" strings
//This is strictly an Android class and does not support all html tags
//Set it in a text view
textView.setText(textSpan);
